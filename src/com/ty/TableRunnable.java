package com.ty;

public class TableRunnable implements Runnable {
    int num;
    Thread threadObj;

    public TableRunnable(int num) {
        this.num = num;
        threadObj = new Thread(this, "multiplication started");
        threadObj.start();
    }
    public void run(){
        for(int iterate=1;iterate<=10;iterate++){
            System.out.println(num*iterate);
        }
    }
}
